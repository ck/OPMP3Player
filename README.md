# OPMP3Player是什么

OPMP3Play是一个JQuery网页在线音乐播放器插件。

播放器采用Flex 4.0开发，结合Javascript前台脚本，实现网页在线歌词的同步。

程序使用很简单，只需简单的初始化即可使用。

# Demo

[http://www.eamonning.com/demo/OPMP3Player/demo.html](http://www.eamonning.com/demo/OPMP3Player/demo.html)

![](http://doc.tecmz.com/data/image/201509/08/18929_uNfW_5401.jpg)


# 如何使用

HTML代码

```html
<div id="OPMP3Player_Container" style="width:100%;"></div>
<div style="border:1px solid #CCC;padding:10px;">
　　<div id="OPMP3Player_LRCBox" style="height:200px;position:relative;overflow:hidden;">
　　</div>
</div>
```

Javascript代码

```javascript
var myPlayer=$('#OPMP3Player_Container').OPMP3Player({
    //Flash播放地址
    flashUrl  :  "OPMP3Player.swf",
    //MP3文件
    mp3Url     :  "example_music.mp3",
    //自动播放
    autoPlay   :   true,
    //LRC歌词，如果该值为空，将忽略所有LRC的操作
    lrcUrl     :   "example_music.lrc",
    //如果同时指定了歌词容器lrcContainer的height=xxPX，overflow=hidden和position=relative
    //并且指定了timeChangeCallback为系统的xxx.timeChangeCallback，歌词将会自动滚动
    lrcContainer : '#OPMP3Player_LRCBox',
    //普通歌词CSS样式
    lrcClass : 'lrcClass',
    //歌词高亮CSS样式
    lrcHighlightClass : 'lrcHighlightClass',
    //时间改变调用函数，可以自定义，我们这里用于更新歌词
    //如果您想全部自己定义，请参照OPMP3Player.js源代码
    timeChangeCallback: "myPlayer.timeChangeCallback"
});
```


# 许可协议

Co.MZ企业系统遵循Apache2开源协议发布。Apache Licence是著名的非盈利开源组织Apache采用的协议。该协议和BSD类似，鼓励代码共享和尊重原作者的著作权，同样允许代码修改，再作为开源或商业软件发布。需要满足的条件:

1. 需要给代码的用户一份Apache Licence ；
2. 如果你修改了代码，需要在被修改的文件中说明；
3. 在延伸的代码中（修改和有源代码衍生的代码中）需要带有原来代码中的协议，商标，专利声明和其他原来作者规定需要包含的说明；
4. 如果再发布的产品中包含一个Notice文件，则在Notice文件中需要带有Apache Licence。你可以在Notice中增加自己的许可，但不可以表现为对Apache Licence构成更改。

具体的协议参考：[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)。